# FreeBSD Manpage Search

A Firefox WebExtension that allows the user to search manual page into official FreeBSD manual page server.
(man.freebsd.org)

- https://addons.mozilla.org/addon/freebsd-manpage/
